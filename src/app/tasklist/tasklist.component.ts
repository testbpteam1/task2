import { Component, OnInit } from '@angular/core';
import { Task } from './task'

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {

  tasks : Task[];
  selectedTask : Task;
  newTaskName : string;

  constructor() { }

  select(task : Task) {
    this.selectedTask = task;
  }

  remove(task : Task) {
    if (this.selectedTask === task) {
      this.selectedTask = null;
    }
    
    let index = this.tasks.indexOf(task);
    if (index >= 0) {
      this.tasks.splice(index, 1);
    }
  }

  add() {
    this.tasks.push({name: this.newTaskName, done: false});
    this.newTaskName = "";
  }

  ngOnInit() {
    this.tasks = [
      { name: "task 01", done: false },
      { name: "task 02", done: false },
      { name: "task 03", done: false },
      { name: "task 04", done: false },
      { name: "task 05", done: false },
      { name: "task 06", done: false },
      { name: "task 07", done: false }
    ];
  }

}
